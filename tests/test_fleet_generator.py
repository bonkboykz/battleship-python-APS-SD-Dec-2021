import unittest

from torpydo.fleet_generator import generate_fleet


class TestFleetGenerator(unittest.TestCase):
    def test_fleet_generator(self):
        fleet = generate_fleet()

        self.assertEquals(len(fleet), 5)
        self.assertEquals(fleet[0].size, 5)
        self.assertEquals(fleet[1].size, 4)
        self.assertEquals(fleet[2].size, 3)
        self.assertEquals(fleet[3].size, 3)
        self.assertEquals(fleet[4].size, 2)


if '__main__' == __name__:
    unittest.main()
