import unittest

from torpydo.fleet_generator import parse_fleet_config


class TestParseFleetConfig(unittest.TestCase):
    def test_parse_fleet_config(self):
        for i in range(1, 6):
            fleet = parse_fleet_config(f'./torpydo/fleet_configs/{i}')
            self.assertEquals(len(fleet), 5)
            self.assertEquals(fleet[0].size, 5)
            self.assertEquals(fleet[1].size, 4)
            self.assertEquals(fleet[2].size, 3)
            self.assertEquals(fleet[3].size, 3)
            self.assertEquals(fleet[4].size, 2)


if '__main__' == __name__:
    unittest.main()
