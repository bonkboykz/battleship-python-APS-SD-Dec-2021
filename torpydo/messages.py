from colorama import Fore, Style

HIT_MESSAGE = r'''
    \          .  ./
  \   .:"";'.:..""   /
     (M^^.^~~:.'"").
-   (/  .    . . \ \)  -
   ((| :. ~ ^  :. .|))
-   (\- |  \ /  |  /)  -
     -\  \     /  /-
       \  \   /  /'''


HIT_STYLE = Fore.RED
MISS_STYLE = Fore.BLUE
DEFAULT_STYLE = Style.RESET_ALL


def print_message(message='', message_type=DEFAULT_STYLE):
    print(message_type + message + DEFAULT_STYLE)
