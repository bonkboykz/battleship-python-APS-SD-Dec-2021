import random

import colorama
from colorama import Fore, Back, Style

from torpydo.game import player_turn
from torpydo.messages import print_message

from torpydo.ship import Color, Letter, Position, Ship
from torpydo.game_controller import GameController
from torpydo.fleet_generator import generate_fleet


myFleet = []
enemyFleet = []

def main():
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet

    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    while True:
        print_message('~~~~~~~~~~~~~~~~~~~~')
        print_message()
        print_message("Player, it's your turn")

        print_message("""Command list:\n
            [H] - Enter next hit coordinates \n
            [L] - Surrender the game \n
            [Q] - Quit the game
        """)
        commandKey = input()
        print_message()
        actions = {
            "H": hit,
            "h": hit,
            "L": lose,
            "l": lose,
            "Q": quit,
            "q": quit,
        }
        execute_next_action = actions.get(commandKey)
        if execute_next_action is None:
            print_message("No such command")
            continue

        execute_next_action()

def hit():
    position = parse_position(input("Enter coordinates for your shot :"))
    player_turn(position, enemyFleet)

    position = get_random_position()
    print_message(f"Computer shoot in {position.column.name}{position.row}")
    player_turn(position, myFleet)

def quit():
    print_message('Quitting the game')
    exit()

def lose():
    print_message("You've lost.")
    exit()

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return position

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")

        for i in range(ship.size):
            position_input = input(f"Enter position {i} of {ship.size} (i.e A3):")

            ship.add_position(position_input)


def initialize_enemyFleet():
    global enemyFleet
    enemyFleet = generate_fleet()


if __name__ == '__main__':
    main()
