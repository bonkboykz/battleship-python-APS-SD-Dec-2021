from torpydo.game_controller import GameController
from torpydo.ship import Position, Letter
import random
import os
import sys


def generate_fleet():
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        config_file_id = 2
    else:
        config_file_id = random.randint(1, 5)
    filepath = f'{os.path.dirname(__file__)}/fleet_configs/{config_file_id}'
    return parse_fleet_config(filepath)


def parse_fleet_config(path):
    positions = []
    with open(path, "r") as f:
        for position in f.readlines():
            if position:
                col = Letter[position[0]]
                row = int(position[1])
                positions.append(Position(col, row))

    fleet = GameController.initialize_ships()

    sheep = 0
    for position in positions:
        if len(fleet[sheep].positions) == fleet[sheep].size:
            sheep += 1

        fleet[sheep].positions.append(position)

    return fleet
