from torpydo.game_controller import GameController
from torpydo.messages import HIT_MESSAGE, print_message, HIT_STYLE, MISS_STYLE


def player_turn(position, enemy_fleet):
    is_hit = GameController.check_is_hit(enemy_fleet, position)
    if is_hit:
        print_message(HIT_MESSAGE, HIT_STYLE)

        print_message("Yeah ! Nice hit!", HIT_STYLE)
    else:
        print_message("Miss", MISS_STYLE)

    print_message()
