from enum import Enum


class Actions(Enum):
    hit = "hit"
    lose = "lose"
    quit = "quit"


